/* Importing the necessary libraries */
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

#define USE_SERIAL Serial
#define buttonInputPin D0
#define IR D1
#define flowSensorInputPin D2
#define ledOutputPin D3
#define relaySignal D4
#define wifiLedOutputPin D5
#define loopCompleteOutputPin D6
#define relayLedOutputPin D7
#define irLedOutoutPin D8

//TODO: If no initial connection, do not do anything.
//TODO: Keep a database of whitelisted WiFi networks.

int buttonState = 0;          // Variable to store the input state of the push button.
byte sensorInterrupt = 0;     // 0 = digital pin 2
volatile byte pulseCount = 0; // Variable to store the pulse count input from the 'Hall effect flow sensor'.

float calibrationFactor = 4.5; // The hall effect flow sensor outputs approximately 4.5 pulses per second per litre/minute of flow.

float flowRate;
unsigned int flowMilliLiters;
unsigned long totalMilliLiters;
unsigned long oldTime;
unsigned int addr = 0;
boolean ir;

struct
{
    unsigned long val = 0;
} litersData;

//BR
boolean one_liter_notif = false;
/* SSID and PASSWORD for the AP */
boolean wifi_soft_ap_on = false;
boolean wifi_station_on = false;
const char *ap_ssid = "ESP8266-0101";
const char *ap_pass = "coca-cola";

String ssid = "";
String pass = "";

IPAddress local_ip(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);

ESP8266WebServer server(80);

int setupWiFiAsStation()
{
    if (wifi_soft_ap_on)
    {
        Serial.print("Turning off soft access point....");
        WiFi.softAPdisconnect(true);
        wifi_soft_ap_on = false;
        WiFi.mode(WIFI_STA);
    }
    WiFi.begin(ssid, pass);
    wifi_station_on = true;
    int count = 0;
    const long wifi_search_duration = 10;

    //TODO: Change this to a time constraint.
    while (WiFi.status() != WL_CONNECTED || WiFi.status() == WL_CONNECT_FAILED || WiFi.status() == WL_NO_SSID_AVAIL)
    {
        digitalWrite(wifiLedOutputPin, HIGH);
        Serial.print(".");
        delay(500);
        count++;
        Serial.print("Time elapsed trying to connect: ");
        Serial.print(count);
        Serial.print(" s. \n");
        digitalWrite(wifiLedOutputPin, LOW);
        delay(500);
        if (count >= wifi_search_duration)
        {
            Serial.println("Trying to go back into AP mode");
            setupWiFiAsAccessPoint();
            return 0;
        }
        digitalWrite(wifiLedOutputPin, LOW);
    }

    digitalWrite(wifiLedOutputPin, HIGH);
    Serial.println("");
    Serial.println("WiFi connected...");
    Serial.println(WiFi.localIP());
    sendMacAddressAndPass();
    return 1;
}

void setupWiFiAsAccessPoint()
{
    digitalWrite(wifiLedOutputPin, LOW);
    WiFi.mode(WIFI_AP);
    wifi_station_on = false;
    WiFi.softAP(ap_ssid, ap_pass);
    WiFi.softAPConfig(local_ip, gateway, subnet);
    wifi_soft_ap_on = true;

    setupServerRoutes();
}

void setupServerRoutes()
{
    server.on("/", connectionEstd);
    server.on("/", notFound);

    server.begin();
    Serial.println("Soft Access Point Established.......");
}
//ER

/* All initial setups */
void setup()
{
    Serial.begin(115200); // Starting the serial communication to send messages to the computer.
    delay(1000);

    //TODO: CHECK THE CODE.
    EEPROM.begin(512); // Starts EEPROM module and allocates 512kb memory.

    pinMode(flowSensorInputPin, INPUT); // Initialization of the variable "inputPin (D2)" as INPUT pin.
    pinMode(buttonInputPin, INPUT);     // Initialization of the variable "buttonPin (D7)" as INPUT pin.
    pinMode(ledOutputPin, OUTPUT);      // Initialization of the variable "ledPin (D4)" as OUTPUT pin.
    pinMode(IR, INPUT);
    pinMode(relaySignal, OUTPUT);
    pinMode(wifiLedOutputPin, OUTPUT);
    pinMode(loopCompleteOutputPin, OUTPUT);
    pinMode(relayLedOutputPin, OUTPUT);
    pinMode(irLedOutoutPin, OUTPUT);

    buttonState = 0;
    pulseCount = 0;
    flowRate = 0.0;
    flowMilliLiters = 0;
    totalMilliLiters = 0;
    oldTime = 0;
    addr = 0;

    //BR
    setupWiFiAsStation();
    //setupWiFiAsAccessPoint();
    delay(1000);
    //setupServerRoutes();

    //ER
    digitalWrite(flowSensorInputPin, HIGH);
    attachInterrupt(digitalPinToInterrupt(flowSensorInputPin), pulseCounter, RISING);
}

void connectionEstd()
{
    Serial.println("Connection established...");
    if (server.method() == HTTP_GET)
    {
        ssid = server.arg("ssid");
        pass = server.arg("pass");
        //server.send(200, "text/html", "<html><title>Soft AP</title><body>SoftAP GOT GET:" + ssid + ".</body></html>");
        server.send(200, "application/json", "{\"success\": \"true\"}");
        Serial.println(ssid);
        Serial.println(pass);

        //stop WiFi AP and start Station
        setupWiFiAsStation();
    }
    else
    {
        server.send(200, "text/html", "I have got naathin to say..");
    }
}

void notFound()
{
    Serial.println("COnnection Failed....");
    server.send(200, "text/html", "<html><title>Soft AP</title><body>SoftAP not found</body></html>");
}

void buildHTML()
{
}

/* The main loop */
void loop()
{
    digitalWrite(loopCompleteOutputPin, LOW);
    server.handleClient();
    //BR
    //setupServerRoutes();
    if (WiFi.status() != WL_CONNECTED && !wifi_soft_ap_on)
    {
        Serial.println("Trying to set up WiFi as Station again...");
        setupWiFiAsStation();
    }
    //ER
    if ((millis() - oldTime) > 1000)
    {

        /* Disabling the interrupt while calculating flow rate and sending the value to the host. */
        detachInterrupt(sensorInterrupt);

        /* Getting the data from EEPROM */
        EEPROM.get(addr, litersData);

        /* Because this loop may not complete in exactly 1 second intervals, we calculate the number of milliseconds that have passed since the last execution and use that to scale the output. */
        /* We also apply the calibrationFactor to scale the output based on the number of pulses per second, per units of measure (litres/minutes in this case) coming from the sensor. */
        flowRate = (((1000.00 / (millis() - oldTime)) * pulseCount) / calibrationFactor);

        /* Because we've disabled interrupts the millis() function won't actually be incrementing right at this point, but it will still return the value it was set to just before interrupts went away. */
        oldTime = millis();

        /* Divide the flow rate in litres/minute by 60 to determine how many litres have passed through the sensor in this 1 second interval, then multiply by 1000 to convert it to millilitres. */
        flowMilliLiters = ((flowRate / 60) * 1000);

        /* Add the millilitres passed in this second to the cumulative total. */
        totalMilliLiters += flowMilliLiters;

        /* Incrementing the value of EEPROM */
        litersData.val += flowMilliLiters;

        //BR
        if (flowMilliLiters > 0)
        {
            digitalWrite(ledOutputPin, HIGH);
            delay(100);
            digitalWrite(ledOutputPin, LOW);
        }
        //ER
        /* Object detection */
        ir = digitalRead(IR);

        if (ir == LOW)
        {
            digitalWrite(irLedOutoutPin, HIGH);
        }
        else
            digitalWrite(irLedOutoutPin, LOW);

        if (ir == LOW)
        {
            digitalWrite(relayLedOutputPin, HIGH);
            digitalWrite(relaySignal, HIGH);
            Serial.println("Object detected & Valve is open.");
            if (litersData.val >= 1000 && litersData.val < 2000)
            {
                digitalWrite(ledOutputPin, HIGH);
                if (one_liter_notif == false)
                {
                    notifyServer(1);
                    one_liter_notif = true;
                }
            }
            if (litersData.val >= 2000)
            {
                digitalWrite(ledOutputPin, LOW);
                one_liter_notif = false;
                litersData.val = 0;
                notifyServer(2);
                Serial.println(" EEPROM has been reset.");
                Serial.println("");
                delay(1000);
            }
        }
        else if (ir == HIGH)
        {
            if (litersData.val >= 1000 && litersData.val < 2000)
            {
                Serial.println("Object not detected & Valve is close.");
                digitalWrite(ledOutputPin, HIGH);
                if (one_liter_notif == false)
                {
                    notifyServer(1);
                    one_liter_notif = true;
                }
            }
            if (litersData.val >= 2000)
            {
                digitalWrite(ledOutputPin, LOW);

                one_liter_notif = false;
                digitalWrite(relayLedOutputPin, LOW);
                digitalWrite(relaySignal, LOW);
                Serial.println("Object not detected & Valve is close.");
                litersData.val = 0;
                notifyServer(2);
                Serial.println(" EEPROM has been reset.");
                Serial.println("");
                delay(1000);
                digitalWrite(ledOutputPin, LOW);
            }
        }

        /* Hard reset the EEPROM if the button is pressed. */
        buttonState = digitalRead(buttonInputPin);
        if (buttonState == HIGH)
        {
            digitalWrite(ledOutputPin, HIGH);
            litersData.val = 0;
            notifyServer(0);
            Serial.println(" EEPROM has been reset.");
            Serial.println("");
            delay(1000);
            digitalWrite(ledOutputPin, LOW);
        }

        unsigned int frac;

        /* Print the previous cumulative total of liters that flowed */
        Serial.print(" Previous output liquid quantity: " + String(litersData.val));
        Serial.println("ml");

        /* Print the flow rate for this second in litres/minute */
        Serial.print(" Flow rate: ");
        Serial.print(int(flowRate));            // Print the integer part of the variable.
        Serial.print(".");                      // Print the decimal point.
        frac = (flowRate - int(flowRate)) * 10; // Determine the fractional part. The 10 multiplier gives 1 decimal place.
        Serial.print(frac, DEC);                // Print the fractional part of the variable.
        Serial.println("l/min");

        /* Print the number of litres flowed in this second */
        Serial.print(" Current Liquid Flowing: ");
        Serial.print(flowMilliLiters);
        Serial.println("ml/sec");

        /* Print the cumulative total of litres flowed since starting */
        Serial.print(" Output Liquid Quantity: ");
        Serial.print(totalMilliLiters);
        Serial.println("ml");

        Serial.println("");

        EEPROM.put(addr, litersData);
        EEPROM.commit();

        /* Reseting the pulse counter so we can start incrementing again */
        pulseCount = 0;
    }
    digitalWrite(loopCompleteOutputPin, HIGH);
    delay(100);
}

/* Function to send notifications to the server */
void notifyServer(int fill_counter)
{
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        /* http.begin("18.217.30.144:9015/serverPing"); */
        if (fill_counter == 1)
        {
            http.begin("3.14.10.167", 9015, "/situationAlert?serverTask=alert&deviceId=testId&fillStat=1"); //+fill_counter);
        }
        else if (fill_counter == 2)
        {
            http.begin("3.14.10.167", 9015, "/situationAlert?serverTask=alert&deviceId=testId&fillStat=2"); //+fill_counter);
        }
        else
        {
            http.begin("3.14.10.167", 9015, "/situationAlert?serverTask=alert&deviceId=testId&fillStat=0"); //+fill_counter);
        }
        //http.begin("3.14.10.167", 9015, "/situationAlert?serverTask=alert&deviceId=testId&fillStat=1");//+fill_counter);
        int httpCode = http.GET();

        Serial.print(" httpCode: ");
        Serial.println(httpCode);
        Serial.println();
        if (httpCode > 0)
        {
            const size_t bufferSize = JSON_OBJECT_SIZE(2);
            DynamicJsonBuffer jsonBuffer(bufferSize);
            JsonObject &root = jsonBuffer.parseObject(http.getString());

            const char *response = root["response"];
            Serial.print(" response: ");
            Serial.println(response);
        }
        http.end();
    }
    delay(1000);
}

void sendMacAddressAndPass()
{
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        String path = "/setupDevice?deviceId=" + WiFi.macAddress() + "&devicePass=" + "coca-cola" + "&currentDeviceToken=online";
        http.begin("3.14.0.167", 9015, path);
        int httpCode = http.GET();
        http.end();
        path = "";
    }
    delay(500);
}

/* Interrupt service routine */
void pulseCounter()
{
    pulseCount++; /* Increment the pulse counter */
}
