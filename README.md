# Battle Of Waterloo Physical Device Documentation
#### Global Variables

| Variables | Type | Description |
|-----------|:------|:-------------|
| button_state | int | Stores the state of the RESET_PUSH_BUTTON |
| sensorInterrupt | byte | Stores interrupt state (attached/detached) |
| pulseCount | byte | Stores the pulse count from the Hall effect flow sensor |
| calibrationFactor | float | The hall effect flow sensor outputs approximately 4.5 pulses per second per litre/minute of flow |
| flowRate | float | Stores the rate of fluid flowing through the flow sensor |
| flowMilliLiters | unsigned int | Stores how many milliliters of fluid has flowed through the sensor in delta time |
| totalMilliLiters | unsigned long | Stores the total amount of fluid that has flowed through the sensor from the time the device was started |
| oldTime | unsigned long | Stores the last time to use in functions for non blocking operations |
| addr | unsigned int | EEPROM memory location |
| ir | boolean | Stores the state of the IR sesnor |
| litersData | struct | Stores the amount of water that has flowed through the sensor |
| litersData.val | unsigned long | Stores the amount of water that has flowed through the sensor |
| one_liter_notif | boolean | Stores if the one liter notification has been sent |
| wifi_soft_ap_on | boolean | Stores if the WiFi is in the Soft Access Point Mode |
| wifi_station_on | boolean | Stores if the WiFi is in the Station Mode |
| ap_ssid | const char* | Stores the default SSID of the WiFi, the router wants to connect to |
| ap_pass | const char* | Stores the default password of the default SSID |
| ssid | String | Stores if a new WiFi SSID is received |
| pass | String | Stores if a new password is received for the new SSID |


### Functions
----
* `int setupWiFiAsStation()` 

    *return:* 1 if the device WiFi could connect to a WiFi Router, else returns a 0 and changes the state of the device WiFi into Access Point mode.

* `void setupWiFiAsAccessPoint()`

    Sets the WiFi module in the device to the Access Point mode to which users will be able to connect and starts up a local server on the device
    The local ip, gateway and the subnet are as follows.
    ```
    IPAddress local_ip(192,168,1,1);
    IPAddress gateway(192,168,1,1);
    IPAddress subnet(255,255,255,0);
    ```

* `void setupServerRoutes()`

    Sets up the /connectionEstd and the /notFound route and starts the server.

* `void setup()`

    Begins the Serial Port, the EEPROM and sets the WiFi module to be in the station mode, attaches Interrupts and sets up the PinModes for the various inputs and the outputs.

* `void connectionEstd()`

    Respond wifi configuration receival report to only GET call from mobile

* `void notFound()`
 
    Respond wifi setup failure report to mobile if no wifi found with given configuration

* `void loop()`
    
    Continously perform following activities to provide real time event management:
    
    * Check if connected to WIFI else setup WIFI station. 
    * Read and calculate water flow, store record and report to server on overflow. 
    * Check if no object found in front of IR sensor to turn on valve. 
    * Reset EEPROM on reset button press. 
    * Print event data and manage LEDs on events
    
* `void notifyServer(int)`

    Setup and send HTTP call to server to send FCM notification to mobiles

    *param:* fill_counter Water overflow amount integer to report to server 

* `void sendMacAddressAndPass()`

    Send physical device MAC IP and Password to server for physical device registration with server when wifi is connected

* `void pulseCounter()`

    Interrupt service routine. Increments the pulse counter. 